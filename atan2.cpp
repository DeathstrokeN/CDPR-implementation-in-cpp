#include "atan2.h"

#include <iostream>

double atan2_custom(double y, double x) {
    // Handle special cases to avoid division by zero
    double M_PI = 3.14;
    if (x > 0) {
        return std::atan(y / x);
    }
    else if (x < 0) {
        if (y >= 0) {
            return std::atan(y / x) + M_PI;
        }
        else {
            return std::atan(y / x) - M_PI;
        }
    }
    else {
        if (y > 0) {
            return M_PI / 2;
        }
        else if (y < 0) {
            return -M_PI / 2;
        }
        else {
            return 0; // Undefined, return 0 in this case
        }
    }
}